# Variables
SHELL = /bin/sh
NAME = virtainer

PREFIX ?= /usr

install:
	install -Dm 775 run.sh "$(DESTDIR)$(PREFIX)/bin/virtainer"

.PHONY: install
